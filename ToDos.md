- Rewrite in stats.py the assocation between numerical and categorical features using the code provided in data_exploration.ipynb (DONE !!!)

- Rewrite display function in modellingtools so it prints plots instead of figures (DONE)

- Rename automatically the metrics used in plot_learning_curve (DONE)

- Complexify the models to boost the performance