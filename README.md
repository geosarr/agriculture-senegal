# Agriculture in Senegal

This project is an ongoing study of dry cereals production (Maize, Millet, Rice, ...) by farm households in Senegal between 2016 an 2017. The data used in the project is a subsample of the D.A.P.S.A. (Direction de l’Analyse, de la Prévision et des Statistiques Agricoles) database.

We adopt a machine learning approach and a nonparametric econometrics approach to understand how cereals production is affected by different factors.
